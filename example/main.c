#include <curses.h>
#include <unistd.h>
#include <fcntl.h>
#include <locale.h>
#include <stdlib.h>
int main(int argc,char *argv [])
{
	if(argc<2){
		return 1;
	}
    setlocale(LC_ALL, "");
    if (!initscr())
    {
        perror("Error initialising curses.");
        return 1;
    }
    noecho();
    curs_set(0);
    WINDOW *page = newwin(LINES - 1, COLS, 0, 0);
    WINDOW *cmd = newwin(1, COLS, LINES - 1, 0);
    int fd = open(argv[1], O_RDONLY);
    wrefresh(page);
    int quit = 0;
    wclear(page);
    char *buf = calloc(1, 1);
    int size = 0;
    while(read(fd, buf, 1))
    {
        size++;
        if(waddstr(page, buf))
        {
            break;
        }
    }
    lseek(fd, -size, SEEK_CUR);
    free(buf);
    wrefresh(page);

    while(!quit)
    {
        switch(wgetch(cmd))
        {
        case 'q':
            delwin(page);
            delwin(cmd);
            endwin();
            quit = 1;
            break;
        }

    }
}
