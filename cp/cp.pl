#!/bin/perl 
use strict;
use Cwd qw/abs_path/;
my $recursive  = 0;
my $force      = 0;
my $all_is_arg = 0;
my @args;

sub cp {
    my ( $src, $dst ) = @_;
    if ( !open ifd, "<$src" ) {
        die "Cannot open $src for reading:$!\n";
    }
    elsif ( !open ofd, ">$dst" ) {
        die "Cannot create $dst:$!\n";
    }
    else {
        while ( my $line = <ifd> ) {
            print ofd $line;
        }
    }
    close ifd;
    close ofd;
}

sub dst_file_exists {
    my ( $src, $dst ) = @_;
    if ($force) {
        cp( $src, $dst );
    }
    else {
        print "cp: overwrite '$dst'?";
        my $line = <STDIN>;
        if ( $line =~ '^[Yy]' ) {
            cp( $src, $dst );
        }
    }
}

sub src_is_file {
    my ( $src, $dst ) = @_;
    if ( -d $dst ) {
        if ( -f "$dst/$src" ) {
            dst_file_exists( $src, "$dst/$src" );
        }
        else {
            cp( $src, "$dst/$src" );
        }
    }
    elsif ( -f $dst ) {
        dst_file_exists( $src, $dst );
    }
    else {
        cp( $src, $dst );
    }
}

sub rec_cp {
    my ( $src, $dst ) = @_;
    if ( mkdir $dst ) {
        if ( opendir DIR, $src ) {
            while ( my $fname = readdir DIR ) {

                if ( -f "$src/$fname" ) {
                    cp( "$src/$fname", "$dst/$fname" );
                }
                elsif ( -d "$src/$fname" ) {
                    if ( $fname !~ '^\.{1,2}$' ) {
                        rec_cp( "$src/$fname", "$dst/$fname" );
                    }
                }
            }
        }
        else {
            die "cp: cannot create $dst:$!";
        }
    }
    else {
        die "cp:cannot open $src:$!";
    }
}

sub src_is_dir {
    my ( $src, $dst ) = @_;
    if ( -d $dst ) {
        if ( abs_path($src) =~ "abs_path($dst).*" ) {
            die
                "cp: cannot copy a directory, '$src', into itself,'$dst'\n";
        }
        else {
            rec_cp( $src, "$dst/$src" );
        }
    }
    elsif ( -f $dst ) {
        die
            "cp: cannot overwrite non-directory '$src' with directory '$dst'";
    }
    else {
        rec_cp( $src, $dst );
    }

}

sub cp_from_src_to_dst {
    my ( $src, $dst ) = @_;
    if ( -e $src ) {
        if ( -f $src ) {
            src_is_file( $src, $dst );
        }
        elsif ( -d $src && $recursive ) {
            src_is_dir( $src, $dst );
        }
        else {
            die "cp:-r not specified; omitting directory $args[0]\n";
        }
    }
    else {
        die "cp: cannot stat $args[0]: No such file or directory\n";
    }
}
for my $arg (@ARGV) {
    if ($all_is_arg) {
        push @args, $arg;
    }
    if ( $arg eq "--" ) {
        $all_is_arg = 1;
    }
    elsif ( $arg eq "-r" ) {
        $recursive = 1;
    }
    elsif ( $arg eq "-f" ) {
        $force = 1;
    }
    elsif ( $arg =~ "^[^-]" ) {
        push @args, $arg;
    }
}
if ( @args >= 2 ) {
    if ( @args == 2 ) {
        cp_from_src_to_dst( $args[0], $args[1] );
    }
    else {
        if ( -d $args[ @args - 1 ] ) {
            for ( my $i = 0; $i < @args - 1; $i++ ) {
                cp_from_src_to_dst( $args[$i], $args[ @args - 1 ] );
            }
        }
        else {
            die "cp:'$args[@args-1]' is not directory\n";
        }
    }
}
else {
    die "Too few argument's count.\n";
}
