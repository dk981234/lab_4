#pragma once
#include <curses.h>
#include <unistd.h>
void resize(context *ctx);
void print_page(context * ctx);
void line_down_file(context *ctx);
int  line_up_file(context *ctx);
void page_up_file(context *ctx);
void page_down_file(context *ctx);
void last_page_file(context *ctx);
void first_page_file(context *ctx);