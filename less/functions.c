#include "context.h"
#include <unistd.h>
#include <stdlib.h>
#include <curses.h>
void resize(context *ctx)
{
    wclear(ctx->page);
    mvwin(ctx->page, 0, 0);
    wresize(ctx->page, LINES - 1, COLS);
    mvwin(ctx->cmd, LINES - 1, 0);
    wresize(ctx->cmd, 1, COLS);
    wrefresh(ctx->page);
    wrefresh(ctx->cmd);

}
void print_page(context *ctx)
{
    ctx->isEnd = 0;
    wclear(ctx->page);
    char *buf = calloc(1, 1);
    int size = 0;
    while(read(ctx->fd, buf, 1))
    {
        size++;
        if(waddnstr(ctx->page, buf, 1))
        {
            break;
        }
    }

    ctx->size = size;
    buf = calloc(1, 1);
    if(!read(ctx->fd, buf, 1))
    {
        ctx->isEnd = 1;
    }
    else
    {
        lseek(ctx->fd, -1, SEEK_CUR);
    }
    lseek(ctx->fd, -size, SEEK_CUR);
    free(buf);
    wrefresh(ctx->page);
}
//прочитать весь символ со второго байта
int  read_down_symbol(context *ctx, unsigned char first_byte)
{
    first_byte = first_byte >> 4;
    switch(first_byte)
    {
    case 12: //1100
    case 13: //1101
        lseek(ctx->fd, 1, SEEK_CUR);
        return 1;
    case 14://1110
        lseek(ctx->fd, 2, SEEK_CUR);
        return 2;
    case 15://1111
        lseek(ctx->fd, 3, SEEK_CUR);;
        return 3;
    }
    return 0;
}
//прочитать весь символ с предпоследнего байта
void  read_up_symbol(context *ctx)
{
    unsigned char *buf = calloc(1, 1);
    while(*buf >> 6 != 3)
    {
        read(ctx->fd, buf, 1);
        fprintf(stderr, "%u\n", *buf);
        if(lseek(ctx->fd, -2, SEEK_CUR) < 0)
        {
            lseek(ctx->fd, -1, SEEK_CUR);
            ctx->isBegin = 1;
        }
    }


}
void line_down_file(context *ctx)
{

    if(!ctx->isEnd)
    {
        int cols = 0;
        unsigned char *buf = malloc(1);
        while(cols < COLS && read(ctx->fd, buf, 1))
        {

            if(*buf >> 6 == 3) //11XXXXXX
            {
                read_down_symbol(ctx, *buf);
            }
            else if(*buf == '\n')
            {
                break;
            }

            cols += 1;
        }
        if(cols < COLS)
        {
            ctx->isEnd = 1;
        }
        free(buf);
        ctx->isBegin = 0;
    }

}
void  is_begin(context *ctx)
{
    if(lseek(ctx->fd, -1, SEEK_CUR) < 0)
    {
        ctx->isBegin = 1;
    }
    else
    {
        ctx->isBegin = 0;
        lseek(ctx->fd, 1, SEEK_CUR);
    }
}
//последняя логическая строка, до текушей позиции
void last_string(context *ctx, int current_pos)
{
    int cols = 0;
    unsigned char *buf = malloc(1);
    int line = lseek(ctx->fd, 0, SEEK_CUR);
    while(lseek(ctx->fd, 0, SEEK_CUR) < current_pos)
    {
        if(cols == 0)
        {
            line = lseek(ctx->fd, 0, SEEK_CUR);
        }
        read(ctx->fd, buf, 1);
        if(*buf >> 6 == 3) //11XXXXXX
        {
            read_down_symbol(ctx, *buf);
        }
        else if(*buf == '\n')
        {
            lseek(ctx->fd, -1, SEEK_CUR);
            break;
        }
        cols += 1;
        if(cols == COLS)
        {
            cols = 0;
        }

    }

    lseek(ctx->fd, line, SEEK_SET);

}
int  line_up_file(context *ctx)
{
    if(!ctx->isBegin)
    {
        int current_pos = lseek(ctx->fd, 0, SEEK_CUR);
        unsigned char *buf = malloc(1);
        lseek(ctx->fd, -1, SEEK_CUR);
        read(ctx->fd, buf, 1);
        if(*buf == '\n')
        {
            lseek(ctx->fd, -1, SEEK_CUR);
            is_begin(ctx);
        }
        else
        {
            lseek(ctx->fd, -1, SEEK_CUR);
        }
        *buf = 0;
        while(!(ctx->isBegin) && *buf != '\n')
        {
            lseek(ctx->fd, -1, SEEK_CUR);
            read(ctx->fd, buf, 1);
            lseek(ctx->fd, -1, SEEK_CUR);
            is_begin(ctx);
            if(*buf == '\n')
            {
                lseek(ctx->fd, 1, SEEK_CUR);
                break;
            }
        }
        last_string(ctx, current_pos);
        is_begin(ctx);
        return 1;
    }
    else
    {
        return 0;
    }
}


void page_up_file(context *ctx)
{
    for(int i = 0; i < LINES - 1 && line_up_file(ctx); i++)
    {

    }
}
void first_page_file(context *ctx)
{
    lseek(ctx->fd, 0, SEEK_SET);
}
void last_page_file(context *ctx)
{
    ctx->isBegin=0;
    lseek(ctx->fd, 0, SEEK_END);
    page_up_file(ctx);
}
void page_down_file(context *ctx)
{
    if(!ctx->isEnd)
    {
        lseek(ctx->fd, ctx->size, SEEK_CUR);
    }
    ctx->isBegin = 0;
}
