#include <curses.h>
#include <stdlib.h>
#include "context.h"
#include <fcntl.h>
#include "functions.h"
#include <locale.h>

int main(int argc, char *argv [])
{
    if(argc == 1)
    {
        write(2, "Too few argument's count.\n", 26);
        return 1;
    }

    setlocale(LC_ALL, "");
    if (!initscr())
    {
        perror("Error initialising curses.");
        return 1;
    }
    noecho();
    curs_set(0);
    refresh();
    WINDOW *page = newwin(LINES - 1, COLS, 0, 0);
    WINDOW *cmd = newwin(1, COLS, LINES - 1, 0);
    //set context for each file
    context *ctx_arr = calloc(sizeof(context), argc);
    for(int i = 1; i < argc; i++)
    {

        int fd = open(argv[i], O_RDONLY);
        if(fd < 0)
        {
            delwin(page);
            delwin(cmd);
            endwin();
            perror("Can't open file for reading.");
            return 1;
        }
        ctx_arr[i - 1].cmd = cmd;
        ctx_arr[i - 1].page = page;
        ctx_arr[i - 1].isEnd = 0;
        ctx_arr[i - 1].isBegin = 1;
        ctx_arr[i - 1].fd = fd;

    }
    context *cur_ctx = &ctx_arr[0];
    wrefresh(cmd);
    wrefresh(page);
    print_page(cur_ctx);
    int context_index = 0;
    int quit = 0;

    while(!quit)
    {
        switch(wgetch(cmd))
        {
        case 'n':
            if(context_index == argc - 2)
            {
                context_index = 0;
                cur_ctx = &ctx_arr[0];
            }
            else
            {
                cur_ctx = &ctx_arr[context_index + 1];
                context_index += 1;
            }
            print_page(cur_ctx);
            break;
        case 'p':
            if(context_index == 0)
            {
                context_index = argc - 2;
                cur_ctx = &ctx_arr[argc - 2];
            }
            else
            {
                cur_ctx = &ctx_arr[context_index - 1];
                context_index -= 1;
            }
            print_page(cur_ctx);
            break;
        case KEY_RESIZE:
            resize(cur_ctx);
            print_page(cur_ctx);
            break;
        case 'k':
            line_up_file(cur_ctx);
            print_page(cur_ctx);
            break;
        case ' ':
            page_down_file(cur_ctx);
            print_page(cur_ctx);
            break;
        case KEY_ENTER:
        case 'g':
            first_page_file(cur_ctx);
            print_page(cur_ctx);
            break;
        case 'G':
            last_page_file(cur_ctx);
            print_page(cur_ctx);
            break;
        case 'b':
            page_up_file(cur_ctx);
            print_page(cur_ctx);
            break;
        case 'j':
            line_down_file(cur_ctx);
            print_page(cur_ctx);
            break;
        case 'q':
            delwin(page);
            delwin(cmd);
            endwin();
            quit = 1;
            break;
        }
    }
    return 0;
}
